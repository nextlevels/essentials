<?php

use \Fruitcake\Cors\HandleCors;
use \NextLevels\Essentials\Http\Middleware\JsonResponse;

\Route::middleware([HandleCors::class, JsonResponse::class])
    ->prefix('api')
    ->namespace('NextLevels\Essentials\Http\Controllers')
    ->group(function () {
        \Route::post('essentials/db-dump', 'BackupController@createDatabaseDump');
        \Route::post('essentials/files-dump', 'BackupController@createFilesDump');
    });
