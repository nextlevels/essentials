<?php namespace NextLevels\Essentials\Listener;

use Backend\Classes\Controller;
use Backend\Classes\NavigationManager;
use Backend\Classes\Skin as AbstractSkin;
use October\Rain\Parse\Yaml;

/**
 * Class PluginEventSubscriber
 */
class PluginEventSubscriber
{

    /**
     * @param \Backend\Classes\Controller $controller
     */
    public function onPageBeforeDisplay(Controller $controller): void
    {
        $origViewPath = $controller->guessViewPath();
        $newViewPath = str_replace(base_path(), '', $origViewPath);
        $newViewPath = $this->getActiveSkin()->skinPath . '/views/' . $newViewPath;

        $controller->addViewPath([$newViewPath]);
    }

    /**
     * @param NavigationManager $navigationManager
     */
    public function onExtendMenu(NavigationManager $navigationManager): void
    {
        $menuYmlPath = $this->getActiveSkin()->skinPath . '/config/menu.yml';

        if (file_exists($menuYmlPath)) {
            $yamlParser = new Yaml();
            $extensionMenus = $yamlParser->parseFile($menuYmlPath);
            foreach ($extensionMenus as $context => $definitions) {
                foreach ($definitions as $menuName => $menu) {
                    $sideMenus = array_get($menu, 'sideMenu', []);
                    array_pull($menu, 'sideMenu');
                    $navigationManager->addMainMenuItem($context, $menuName, $menu);
                    $navigationManager->addSideMenuItems($context, $menuName, $sideMenus);
                }
            }
        }
    }

    /**
     * @param $events
     */
    public function subscribe($events): void
    {
        $events->listen('backend.page.beforeDisplay', [$this, 'onPageBeforeDisplay']);
        $events->listen('backend.menu.extendItems', [$this, 'onExtendMenu']);
    }

    /**
     * @return AbstractSkin
     */
    private function getActiveSkin(): AbstractSkin
    {
        return AbstractSkin::getActive();
    }
}
