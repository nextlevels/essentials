<?php namespace NextLevels\Essentials\ValueObject;

use October\Rain\Database\Collection;

/**
 * Class JsonResponse
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>
 */
class JsonResponse
{

    /**
     * @var bool
     */
    public $success = false;

    /**
     * @var bool
     */
    public $error = false;

    /**
     * @var string
     */
    public $errorMessage;

    /**
     * @var int
     */
    public $errorCode = 0;
    public $count = 0;

    /**
     * @var string
     */
    public $responseType;

    /**
     * @var mixed
     */
    public $response;

    /**
     * ResponseContainer constructor.
     *
     * @param $data
     */
    public function __construct($data = null)
    {
        if (isset($data['success'])) {
            $this->success = true;

            if ($data['response'] !== null) {
                $this->setData($data['response']);

                if (\is_array($data['response'])) {
                    $this->count = \count($data['response']);
                }

                if ($data['response'] instanceof Collection) {
                    $this->count = $data['response']->count();
                }
            }
        }

        if (isset($data['errorMessage'])) {
            $this->setError($data['errorMessage'], $data['errorCode']);
        }
    }

    /**
     * @param $data
     *
     * @return $this
     */
    public function setData($data): self
    {
        $this->response = $data;

        return $this;
    }

    /**
     * @param     $errorMessage
     * @param int $errorCode
     */
    public function setError($errorMessage, $errorCode = 500): void
    {
        $this->error = true;
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this);
    }
}
