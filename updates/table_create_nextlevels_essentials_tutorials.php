<?php namespace NextLevels\Essential\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsEssentialsTutorials extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_essentials_tutorials', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('name')->nullable();
            $table->string('controller')->nullable();
            $table->text('content');
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_essentials_tutorials');
    }
}
