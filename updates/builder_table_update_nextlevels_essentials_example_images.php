<?php namespace NextLevels\Essentials\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsEssentialsExampleImages extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_essentials_example_images', function($table)
        {
            $table->string('controller')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('nextlevels_essentials_example_images', function($table)
        {
            $table->dropColumn('controller');
        });
    }
}
