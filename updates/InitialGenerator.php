<?php namespace NextLevels\Essentials\Updates;

use Artisan;
use Exception;
use Log;
use October\Rain\Database\Updates\Seeder;

/**
 * Class InitialGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class InitialGenerator extends Seeder
{

    /**
     * Run generator
     */
    public function run(): void
    {
        try {
            Artisan::call('ide-helper:generate');
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
