<?php namespace NextLevels\Essentials\Http\Controllers;

use Carbon\Carbon;

/**
 * Class BackupController
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>
 */
class BackupController
{

    /**
     * @var string
     */
    protected static $ll = 'nextlevels.essentials::lang.api.backup.';

    /**
     * Validate secret key
     *
     * @param array $data
     *
     * @throws \ValidationException
     */
    protected static function validateSecretKey(array $data): void
    {
        $rules = ['secret_key' => 'required'];
        $attributeNames = ['secret_key' => \Lang::get(self::$ll . 'fields.secret_key')];
        $validator = \Validator::make($data, $rules, [], $attributeNames);

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        if ($data['secret_key'] !== \Config::get('app.key')) {
            throw new \InvalidArgumentException(\Lang::get(self::$ll . 'messages.invalid_secret_key'));
        }
    }

    /**
     * Get formatted filename
     *
     * @param string $secretKey
     * @param string $type
     *
     * @return string
     */
    protected static function getFilename(string $secretKey, string $type = 'db')
    {
        return sprintf(
            '%s_%s_%s_%s.zip',
            $type,
            $secretKey,
            \Config::getEnvironment(),
            Carbon::now()->getTimestamp()
        );
    }

    /**
     * Create backup
     *
     * @param string $secretKey
     * @param string $type
     */
    protected static function createBackup(string $secretKey, string $filename, string $type = 'db'): void
    {
        $command = sprintf('backup:run --only-%s --filename=%s --disable-notifications', $type, $filename);

        try {
            \Artisan::call($command);
        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
        }

        /**
         * @TODO Add call to send projects management software information about backup
         */
    }

    /**
     * Create database dump
     *
     * @param \Request $request
     *
     * @throws \ValidationException
     */
    public function createDatabaseDump(\Request $request): string
    {
        ini_set('max_execution_time', 600);

        self::validateSecretKey($request::all());

        $filename = self::getFilename($request::get('secret_key'));

        self::createBackup($request::get('secret_key'), $filename);

        return $filename;
    }

    /**
     * Create files dump
     *
     * @param \Request $request
     *
     * @throws \ValidationException
     */
    public function createFilesDump(\Request $request): string
    {
        ini_set('max_execution_time', 600);

        self::validateSecretKey($request::all());

        $filename = self::getFilename($request::get('secret_key'), 'files');

        self::createBackup($request::get('secret_key'), $filename, 'files');

        return $filename;
    }
}
