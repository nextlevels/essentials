<?php namespace NextLevels\Essentials\Http\Middleware;

use Nextlevels\Essentials\ValueObject\JsonResponse as JsonValueObject;

/**
 * Class JsonResponse
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>
 */
class JsonResponse
{

    /**
     * @param          $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        /** @var \Illuminate\Http\JsonResponse $response */
        $response = $next($request);

        if (null === $response->exception) {
            $response->setContent(new JsonValueObject([
                'response' => $response->getOriginalContent(),
                'success'  => true
            ]));
        } else {
            $exception = $response->exception;

            $response->setContent(new JsonValueObject([
                'errorMessage' => $exception->getMessage(),
                'errorCode'    => $exception->getCode()
            ]));
        }

        $response->headers->set('content-type', 'application/json');

        return $response;
    }
}
