<?php namespace NextLevels\Essentials\Models;

use Model;
use October\Rain\Database\Builder;
use October\Rain\Database\Collection;
use October\Rain\Database\Traits\Validation;
use System\Models\File;

/**
 * Class ExampleImage
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>
 */
class ExampleImage extends Model
{
    use Validation;

    /**
     * @var string[]
     */
    protected $casts = ['is_active' => 'boolean'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_essentials_example_images';

    /**
     * @var array Validation rules
     */
    public $rules = ['controller' => 'required', 'images' => 'required'];
    public $attributeNames = [
        'controller' => 'nextlevels.essentials::lang.models.example_image.controller',
        'images'     => 'nextlevels.essentials::lang.models.example_image.images',
    ];

    /**
     * @var string[] attachments
     */
    public $attachMany = ['images' => File::class];

    /**
     * Get active records
     *
     * @param Builder $builder
     * @param bool    $status
     */
    public function scopeIsActive(Builder $builder, bool $status = true): void
    {
        $builder->where('is_active', $status);
    }

    /**
     * Get example images based on url, action and role
     *
     * @param string $url
     *
     * @return null|Collection
     */
    public static function getExampleImages(string $url): ?Collection
    {
        $exampleImage = self::whereRaw("? LIKE CONCAT('%', `controller`, '%')", [$url])->first();

        if ($exampleImage !== null) {
            return $exampleImage->images;
        }

        return null;
    }
}
