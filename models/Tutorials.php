<?php namespace NextLevels\Essentials\Models;

use Illuminate\Support\Collection;
use Model;

/**
 * Class Tutorials
 */
class Tutorials extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_essentials_tutorials';

    /**
     * @var array
     */
    protected $jsonable = ['content'];

    /**
     * Get tutorials based on url, action and role
     *
     * @param string $url
     * @param string $action
     * @param string|null $role
     *
     * @return \Illuminate\Support\Collection|\October\Rain\Support\Collection|null
     */
    public static function getTutorial(string $url, string $action, string $role = null)
    {
        $tutorial = self::whereRaw("? LIKE CONCAT('%', `controller`, '%')", [$url])->first();

        if ($tutorial !== null) {
            $collection = collect($tutorial->content);

            return $collection->filter(function ($item) use ($action, $role) {
                return ($item['action'] === $action
                        || $item['action'] === 'all'
                        || ($action === 'create' && $item['action'] === 'update'))
                    && ($item['user_role'] === $role
                        || $item['user_role'] === 'all');
            });
        }

        return null;
    }
}
