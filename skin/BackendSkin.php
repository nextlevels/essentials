<?php namespace NextLevels\Essentials\Skin;

use Backend\Classes\Skin;
use Cms\Classes\Theme;

/**
 * Class BackendSkin
 */
class BackendSkin extends Skin
{

    /**
     * BackendSkin constructor.
     */
    public function __construct()
    {
        $basePath = base_path();
        $this->skinPath = $this->defaultSkinPath = $basePath . '/modules/backend';
        $this->publicSkinPath = $this->defaultPublicSkinPath = \File::localToPublic($this->skinPath);
        $skin = $this->getSkin();

        if ($skin !== 'octobercms'
            && isset($_SERVER['REQUEST_URI'])
            && strpos($_SERVER['REQUEST_URI'], 'backend/rainlab/builder') === false
        ) {
            if (is_dir($basePath . '/themes/nextlevels-backend')) {
                $this->skinPath = $basePath . '/themes/nextlevels-backend';
            } else {
                $this->skinPath = $basePath . '/themes/backend';
            }

            $this->publicSkinPath = \File::localToPublic($this->skinPath);
        }
    }

    /**
     * Get skin
     *
     * @return mixed|string
     */
    public function getSkin()
    {
        if (\Input::has('_skin')) {
            \Cookie::forget('backend_skin');

            $skin = \Input::get('_skin');

            \Cookie::queue('backend_skin', $skin, 1);

            return $skin;
        }

        if (\Cookie::has('backend_skin')) {
            return \Cookie::get('backend_skin');
        }

        return Theme::getActiveTheme()->getDirName();
    }

    /**
     * @return array
     */
    public function skinDetails(): array
    {
        return [
            'name' => 'Customizable Skin'
        ];
    }
}
