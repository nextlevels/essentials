# Next Levels essentials #
`OctoberCMS Plugin to integrate development essentials`

## Laravel IDE Helper ##
### Setup:
- After install run `php artisan october:up` or `php artisan ide-helper:generate` to generate ide-helper file
- Add the package to the `extra.laravel.dont-discover` key in `composer.json`, e.g.
  ```json
  "extra": {
    "laravel": {
      "dont-discover": [
        "barryvdh/laravel-ide-helper"
      ]
    }
  }
  ```