<?php namespace NextLevels\Essentials\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Classes\Controller;

/**
 * Class TutorialController
 */
class TutorialController extends Controller
{

    /**
     * @var string[] behaviors
     */
    public $implement = [ListController::class, FormController::class];

    /**
     * @var string behavior config files
     */
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
}
