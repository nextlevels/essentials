<?php namespace NextLevels\Essentials\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Classes\Controller;

/**
 * Class ExampleImages
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>
 */
class ExampleImages extends Controller
{

    /**
     * @var string[] behaviors
     */
    public $implement = [ListController::class, FormController::class];

    /**
     * @var string behavior config files
     */
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
}
