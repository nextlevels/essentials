<?php namespace NextLevels\Essentials\ReportWidgets;

use Backend\Classes\ReportWidgetBase;

/**
 * Class BaseIconWidget
 *
 * @author Slawa Ditzel <s.d@next-levels.de>
 */
abstract class BaseIconWidget extends ReportWidgetBase
{

    /**
     * Renders the widget.
     */
    public function render()
    {
        $this->addCss([
            plugins_path('nextlevels/essentials/reportwidgets/baseiconwidget/assets/scss/widget.scss')
        ]);

        $this->addViewPath($this->guessViewPathFrom(self::class) . '/partials');

        try {
            $this->loadData();
        } catch (Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        return $this->makePartial('widget');
    }

    /**
     * On render
     *
     * @return bool|mixed|string
     * @throws \Exception
     */
    public function loadData()
    {
        $series = $this->getSeries();
        $seriesData = $this->getData();

        $this->vars['icon'] = $series['icon'];
        $this->vars['name'] = $series['name'];
        $this->vars['data'] = json_encode($seriesData);
    }

    /**
     * Set Series
     */
    public function getSeries()
    {
        return [
                'name' => 'Name',
                'icon' => 'icon-clipboard',
                'data' => 0,
            ];
    }

    /**
     * These operations have to be implemented in subclasses.
     */
    abstract protected function getData(): int;
}
