<?php namespace NextLevels\Essentials\ReportWidgets;

use Backend\Classes\ReportWidgetBase;

/**
 * Class BaseLineChartWidget
 *
 * @author Slawa Ditzel <s.d@next-levels.de>
 */
abstract class BasePieChartWidget extends ReportWidgetBase
{

    /**
     * Renders the widget.
     */
    public function render()
    {
        $this->addCss([
            plugins_path('nextlevels/essentials/reportwidgets/basepiechartwidget/assets/scss/widget.scss')
        ]);

        $this->addViewPath($this->guessViewPathFrom(self::class) . '/partials');
        $this->vars['random'] = rand(10000, getrandmax());

        try {
            $this->loadData();
        } catch (Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        return $this->makePartial('widget');
    }

    /**
     * On render
     *
     * @return bool|mixed|string
     * @throws \Exception
     */
    public function loadData()
    {
        $seriesArray = $this->getSeries();
        $seriesColors = [];
        $seriesLabels = [];
        $seriesData = [];
        foreach ($seriesArray as &$serie) {
            $seriesCount = $this->getData($serie['code']);
            $serie['data'] =  $seriesCount;
            array_push($seriesColors, $serie['color']);
            array_push($seriesLabels, $serie['name']);
            array_push($seriesData, $seriesCount);
        }

        $this->vars['headline'] = $this->getHeadline();
        $this->vars['series'] = $seriesArray;
        $this->vars['seriesData'] = json_encode($seriesData);
        $this->vars['colors'] = json_encode($seriesColors);
        $this->vars['labels'] = json_encode($seriesLabels);
    }

    /**
     * Set Headline
     */
    protected function getHeadline(): string
    {
        return 'Statistik';
    }

    /**
     * Set Series
     */
    public function getSeries()
    {
        return [
             [
                'name' => 'Name',
                'color' => '#0080ff',
                'code' => 'code',
                'data' => 0,
            ]
        ];
    }

    /**
     * These operations have to be implemented in subclasses.
     */
    abstract protected function getData($seriesCode): int;
}
