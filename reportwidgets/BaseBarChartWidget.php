<?php namespace NextLevels\Essentials\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use October\Rain\Exception\ApplicationException;

/**
 * Class BaseBarChartWidget
 *
 * @author Slawa Ditzel <s.d@next-levels.de>
 */
abstract class BaseBarChartWidget extends ReportWidgetBase
{

    /**
     * Renders the widget.
     */
    public function render()
    {
        $this->addCss([
            plugins_path('nextlevels/essentials/reportwidgets/basebarchartwidget/assets/scss/widget.scss')
        ]);

        $this->addViewPath($this->guessViewPathFrom(self::class) . '/partials');
        $this->vars['random'] = rand(10000, getrandmax());

        try {
            $this->loadData();
        } catch (Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        return $this->makePartial('widget');
    }

    public function defineProperties()
    {
        return [
            'period' => [
                'title' => 'Monate',
                'default' => 7,
                'type' => 'dropdown',
            ]
        ];
    }

    public function getPeriodOptions()
    {
        return [7 => '6', 12 => '12', 24 => '24'];
    }

    /**
     * On render
     *
     * @throws \Exception
     */
    public function loadData()
    {
        $days = $this->property('period');
        if (!$days) {
            throw new ApplicationException('Invalid days value: ' . $days);
        }

        $seriesArray = $this->getSeries();
        $seriesColors = [];
        foreach ($seriesArray as &$serie) {
            $seriesCount = $this->getDataSeriesForDays($days, $serie['code']);
            $serie['data'] =  $seriesCount;
            $serie['total'] = array_sum($seriesCount);
            array_push($seriesColors, $serie['color']);
        }

        $this->vars['headline'] = $this->getHeadline();
        $this->vars['series'] = $seriesArray;
        $this->vars['seriesData'] = json_encode($seriesArray);
        $this->vars['colors'] = json_encode($seriesColors);
        $this->vars['timeline'] = json_encode(self::getWeekTimelineArray($days));
    }

    /**
     * Get Data for Series for amount of days
     *
     * @return array
     * @throws \Exception
     */
    protected function getDataSeriesForDays($month, $seriesCode): array
    {
        $dataArray = [];
        $period = self::getWeekTimelinePeriod($month);

        foreach ($period as $date) {
            array_push($dataArray, $this->getDataByDate($date, $seriesCode));
        }

        return $dataArray;
    }

    /**
     * Get Timeline for Week
     *
     * @return array
     * @throws \Exception
     */
    protected static function getWeekTimelineArray($month): array
    {
        $dataArray = [];
        $period = self::getWeekTimelinePeriod($month);

        foreach ($period as $date) {
            array_push($dataArray, $date->format('F'));
        }

        return $dataArray;
    }

    /**
     * Get Period for Week
     *
     * @return CarbonPeriod
     * @throws \Exception
     */
    protected static function getWeekTimelinePeriod($month): CarbonPeriod
    {
        return new CarbonPeriod(Carbon::now()->subMonths($month), CarbonInterval::months(1), Carbon::now());
    }

    /**
     * Set Headline
     */
    protected function getHeadline(): string
    {
        return 'Statistik';
    }

    /**
     * Set Series
     */
    public function getSeries()
    {
        return [
                'name' => 'Name',
                'color' => '#0080ff',
                'code' => 'code',
                'data' => [],
                'total' => 0,
        ];
    }

    /**
     * These operations have to be implemented in subclasses.
     */
    abstract protected function getDataByDate($date, $seriesCode): int;
}
