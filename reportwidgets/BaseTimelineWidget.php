<?php namespace NextLevels\Essentials\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use October\Rain\Database\Collection;

/**
 * Class BaseTimelineWidget
 *
 * @author Slawa Ditzel <s.d@next-levels.de>
 */
abstract class BaseTimelineWidget extends ReportWidgetBase
{

    /**
     * Renders the widget.
     */
    public function render()
    {
        $this->addCss([
            plugins_path('nextlevels/essentials/reportwidgets/basetimelinewidget/assets/scss/widget.scss')
        ]);

        $this->addViewPath($this->guessViewPathFrom(self::class) . '/partials');

        try {
            $this->loadData();
        } catch (Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        return $this->makePartial('widget');
    }

    /**
     * On render
     *
     * @return bool|mixed|string
     * @throws \Exception
     */
    public function loadData()
    {
        $seriesData = $this->getData();

        $this->vars['headline'] = $this->getHeadline();

        $this->vars['series'] = $seriesData;
    }

    /**
     * Set Headline
     */
    protected function getHeadline(): string
    {
        return 'AKTIVITÄTEN FEED';
    }

    /**
     * Set Series
     */
    public function getSeries()
    {
        return [
                'name' => 'Name',
                'labels' => ['headline', 'headline2'],
                'data' => 0,
            ];
    }

    /**
     * These operations have to be implemented in subclasses.
     */
    abstract protected function getData(): Collection;
}
