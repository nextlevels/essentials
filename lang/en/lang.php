<?php return [
    'plugin'      => [
        'name'        => 'Essentials',
        'description' => 'Next Levels development essentials',
    ],
    'fields'      => [
        'is_active'  => 'Aktiv?',
        'images'     => 'Bilder',
        'id'         => 'ID',
        'created_at' => 'Erstellt am',
        'updated_at' => 'Geändert am',
    ],
    'messages' => [
        'impersonate_info' => 'Successfully logged in as user. Log out or click the X to return.'
    ],
    'models'      => [
        'example_image' => [
            'label'              => 'Beispielbild',
            'label_multi'        => 'Beispielbilder',
            'title'              => 'Titel',
            'controller'         => 'Pfad',
            'controller_comment' => 'Zum Beispiel "plugin/controller"',
        ],
        'tutorial'      => [
            'label'              => 'Tutorial',
            'label_multi'        => 'Tutorials',
            'name'               => 'Name',
            'controller'         => 'Pfad',
            'controller_comment' => 'Zum Beispiel "plugin/controller"',
            'content'            => [
                'label'             => 'Tutorial',
                'prompt'            => 'Tutorial hinzufügen',
                'tab'               => 'Tutorials',
                'content'           => 'Überschrift',
                'action'            => 'Ansicht',
                'action_options'    => [
                    'all'    => 'Alle',
                    'index'  => 'Liste',
                    'update' => 'Details'
                ],
                'user_role'         => 'Benutzer Rolle',
                'user_role_options' => [
                    'all' => 'Alle'
                ],
                'html'              => 'Inhalt'
            ]
        ],
    ],
    'permissions' => [
        'example_images' => [
            'label' => 'Beispielbilder verwalten',
            'tab'   => 'Backend'
        ],
        'tutorials'      => [
            'label' => 'Tutorials verwalten',
            'tab'   => 'Backend'
        ]
    ],
    'settings'    => [
        'example_images' => [
            'label'       => 'Beispielbilder',
            'description' => 'Beispielbilder verwalten',
            'category'    => 'Backend'
        ],
        'tutorials'      => [
            'label'       => 'Tutorials Sidebar',
            'description' => 'Tutorials verwalten',
            'category'    => 'Backend'
        ]
    ]
];
