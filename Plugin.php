<?php namespace NextLevels\Essentials;

use App;
use Backend\Classes\Controller;
use Backend\Classes\Skin;
use Backend\Classes\WidgetBase;
use Backend\Facades\Backend;
use Barryvdh\Debugbar\Facade;
use Barryvdh\Debugbar\ServiceProvider;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Config;
use Event;
use Illuminate\Foundation\AliasLoader;
use NextLevels\Essentials\Listener\PluginEventSubscriber;
use NextLevels\Essentials\Skin\BackendSkin;
use Spatie\Backup\BackupServiceProvider;
use System\Classes\PluginBase;

/**
 * Class Plugin
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class Plugin extends PluginBase
{

    /**
     * @var bool
     */
    public $elevated = true;

    /**
     * @return mixed
     */
    protected function getActiveSkin()
    {
        return Skin::getActive();
    }

    /**
     * Boot.
     *
     * @return array|void
     */
    public function boot()
    {
        App::register(ServiceProvider::class);
        App::register(BackupServiceProvider::class);
        App::register(IdeHelperServiceProvider::class);

        if (! config('backend')) {
            \Artisan::call('vendor:publish --provider="Spatie\Backup\BackupServiceProvider"');
        }

        Config::set('nextlevels.essentials::backup.APP_NAME', Config::get('app.name'));
        Config::set('backup', Config::get('nextlevels.essentials::backup'));
        Config::set('backup.APP_NAME', Config::get('app.name'));
        Config::set('cms.backendSkin', BackendSkin::class);
        Config::set('filesystems.disks', array_merge(Config::get('filesystems.disks'), [
            'sftp' => [
                'driver'   => 'sftp',
                'host'     => 'p599600.webspaceconfig.de',
                'username' => 'p599600',
                'password' => 'Ax43!uetjg-jmq',
                'port'     => 22,
                'root'     => '/html/master/web/storage/app/backups_public',
            ],
        ]));
        Event::subscribe(new PluginEventSubscriber);

        Event::listen('backend.page.beforeDisplay', function (Controller $controller) {
            $controller->addDynamicMethod('onUpdateLang', function () {
                \Session::put('lang', post('lang'));

                return \redirect()->refresh();
            });
        });

        Event::listen('backend.page.beforeDisplay', function (Controller $controller) {
            $controller->addDynamicMethod('onUpdateLang', function () {
                \Session::put('lang', post('lang'));

                return \redirect()->refresh();
            });

            $controller->addDynamicMethod('onStopImpersonate', function () {
                if (! \BackendAuth::isImpersonator()) {
                    \BackendAuth::logout();
                } else {
                    \BackendAuth::stopImpersonate();
                }

                \Flash::success(\Lang::get('rainlab.user::lang.session.stop_impersonate_success'));

                return \redirect()->refresh();
            });
        });

        $alias = AliasLoader::getInstance();
        $alias->alias('Debugbar', Facade::class);

        if (! \in_array(Config::getEnvironment(), ['dev', 'development'])) {
            \Debugbar::disable();
        }

        WidgetBase::extendableExtendCallback(function (WidgetBase $widget) {
            $origViewPath = $widget->guessViewPath();
            $newViewPath = str_replace(base_path(), '', $origViewPath);
            $newViewPath = $this->getActiveSkin()->skinPath . '/views/' . $newViewPath . '/partials';

            $widget->addViewPath($newViewPath);
        });
    }

    /**
     * @return array
     */
    public function registerPermissions(): array
    {
        return [
            'nextlevels.essentials.manage_example_images'   => [
                'label' => \Lang::get('nextlevels.essentials::lang.permissions.example_images.label'),
                'tab'   => \Lang::get('nextlevels.essentials::lang.permissions.example_images.tab'),
                'order' => 50,
            ],
            'nextlevels.essentials.manage_tutorial_sidebar' => [
                'label' => \Lang::get('nextlevels.essentials::lang.permissions.tutorials.label'),
                'tab'   => \Lang::get('nextlevels.essentials::lang.permissions.tutorials.tab'),
                'order' => 50,
            ]
        ];
    }

    /**
     * Register settings
     *
     * @return array[]
     */
    public function registerSettings(): array
    {
        return [
            'example_images' => [
                'label'       => \Lang::get('nextlevels.essentials::lang.settings.example_images.label'),
                'description' => \Lang::get('nextlevels.essentials::lang.settings.example_images.description'),
                'category'    => \Lang::get('nextlevels.essentials::lang.settings.example_images.category'),
                'icon'        => 'icon-question',
                'iconSvg'     => 'themes/backend/assets/images/svg-icon/layouts.svg',
                'url'         => Backend::url('nextlevels/essentials/exampleimages'),
                'order'       => 500,
                'permissions' => ['nextlevels.essentials.manage_example_images']
            ],
            'tutorials'      => [
                'label'       => \Lang::get('nextlevels.essentials::lang.settings.tutorials.label'),
                'description' => \Lang::get('nextlevels.essentials::lang.settings.tutorials.description'),
                'category'    => \Lang::get('nextlevels.essentials::lang.settings.tutorials.category'),
                'icon'        => 'icon-question',
                'iconSvg'     => 'plugins/essentials/assets/images/icons/question.svg',
                'url'         => Backend::url('nextlevels/essentials/tutorialcontroller'),
                'order'       => 500,
                'permissions' => ['nextlevels.essentials.manage_tutorial_sidebar']
            ]
        ];
    }
}
